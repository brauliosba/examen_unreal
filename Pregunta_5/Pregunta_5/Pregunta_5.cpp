#include <iostream>
#include <math.h> 

using namespace std;

#define PI 3.14159265
#define maxVisionRange 20.0
#define visionAngle 60.0

struct Vector3 {
    float x, y, z = 0;
    Vector3 (float x, float y, float z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }
};

struct Enemy{
    Vector3* pos;
    bool isRendered = false;
    Enemy (Vector3* pos, bool isRendered) {
        this->pos = pos;
        this->isRendered = isRendered;
    }
};

void isInVisionArea(Vector3* playerPos, Vector3* dir, Enemy* enemy, float values[]) {
    Vector3* posEnemy = new Vector3(enemy->pos->x - playerPos->x, enemy->pos->y - playerPos->y, enemy->pos->z - playerPos->z);

    float dot = dir->x * posEnemy->x + dir->y * posEnemy->y + dir->z * posEnemy->z;

    float magDir = sqrt(pow(dir->x, 2) + pow(dir->y, 2) + pow(dir->z, 2));
    float magPosEnemy = sqrt(pow(posEnemy->x, 2) + pow(posEnemy->y, 2) + pow(posEnemy->z, 2));

    values[0] = (float)acos(dot / (magDir * magPosEnemy)) * 180.0 / PI;
    values[1] = magPosEnemy;
}

void lockOnSystem(Vector3* playerPos, Vector3* pForward, Enemy* activeEnemies[], int size) {
    float values[2] = { 0 ,0 };
    bool cond = false;
    Vector3* dir = new Vector3(pForward->x - playerPos->x, pForward->y - playerPos->y, pForward->z - playerPos->z);


    for (int i = 0; i < size; i++) {
        if (activeEnemies[i]->isRendered) {
            isInVisionArea(playerPos, dir, activeEnemies[i], values);
            if (values[0] < visionAngle / 2 && values[1] < maxVisionRange) {
                cond = true;
                cout << "El enemigo " << (i + 1) << " tiene probaiblidad de ser escogido" << endl;
            }
        }
    }

    if (cond == false) {
        dir = new Vector3(dir->x * -1, dir->y * -1, dir->z * -1);
        for (int i = 0; i < size; i++) {
            if (activeEnemies[i]->isRendered) {
                isInVisionArea(playerPos, dir, activeEnemies[i], values);
                if (values[0] < visionAngle / 2 && values[1] < maxVisionRange) {
                    cond = true;
                    cout << "El enemigo " << (i + 1) << " que esta detras del jugador tiene probaiblidad de ser escogido" << endl;
                }
            }
        }
;    }
}

int main()
{
    Vector3* playerPos = new Vector3(10.0, 5.0, 0.0);
    Vector3* pForward = new Vector3(10.0, 7.0, 0.0);

    Enemy* enemy1 = new Enemy(new Vector3(10.0, 2.0, 0.0), true);
    Enemy* enemy2 = new Enemy(new Vector3(10.0, 35.0, 0.0), true);
    Enemy* enemy3 = new Enemy(new Vector3(10.0, 7.0, 0.0), false);
    Enemy* enemy4 = new Enemy(new Vector3(12.0, 15.0, 0.0), true);
    Enemy* enemy5 = new Enemy(new Vector3(15.0, 22.0, 0.0), true);
    Enemy* activeEnemies[5] = { enemy1, enemy2, enemy3, enemy4, enemy5};

    lockOnSystem(playerPos, pForward, activeEnemies, 5);
    return 0;
}