# Examen_Unreal

Postulante: Braulio Sebastián Baldeón Albornoz

## Instrucciones sobre cómo ejecutar el código fuente

Las preguntas 1, 6 y 7 fueron respondidas en archivos txt.

Con respecto a las demás preguntas es suficiente con tener instalado Visual Studio 2019.

Para la ejecución seguir los siguientes pasos:

- Abrir el archivo .sln de la pregunta que se desea ejecutar.
- Una vez que Visual Studio abrió, presinar ctrl + F5.



