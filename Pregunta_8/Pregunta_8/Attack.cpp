#include "Attack.h"

Attack::Attack(float attackDuration, vector<string> attackTags, vector<string> reactionTags) {
	this->attackDuration = attackDuration;
	this->attackTags = attackTags;
	this->reactionTags = reactionTags;
}