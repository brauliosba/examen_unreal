#pragma once
#include "Attack.h"

#pragma once
class Enemy
{
private:
	string name;
	string enemyType;
	Attack* attacks;
	bool canAttack = true;

public:
	Enemy(string name, string enemyType, Attack* attacks);
	string getName() { return name; }
	string getEnemyType() { return enemyType; }
	Attack* getEnemyAttacks() { return attacks; }
	bool getCanAttack() { return canAttack; }
	void setCanAttack(bool canAttack) { this->canAttack = canAttack; }
};

