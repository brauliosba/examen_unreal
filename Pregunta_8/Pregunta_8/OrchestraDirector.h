#pragma once
#include <iostream>
#include <ctime>
#include "Enemy.h"

class OrchestraDirector
{
private:
	int N;
	int R;
	float T;
	bool orchestraAlive = false;
	bool canActiveOrchestra = true;

public:
	OrchestraDirector(int N, int R, float T);
	bool getOrchestraAlive() { return orchestraAlive; }
	bool getCanActiveOrchestra() { return canActiveOrchestra; }
	void setOrchestraAlive(bool orchestraAlive) { this->orchestraAlive = orchestraAlive; }
	void setCanActiveOrchestra(bool canActiveOrchestra) { this->canActiveOrchestra = canActiveOrchestra; }
	bool allowAttack(Enemy* enemy);
	void sendMessage(Enemy* enemy, bool canAttack);
	bool allowOrchestedAttack();
};

