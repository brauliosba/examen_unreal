#include "OrchestraDirector.h"

OrchestraDirector::OrchestraDirector(int N, int R, float T) {
	this->N = N;
	this->R = R;
	this->T = T;
}

bool OrchestraDirector::allowAttack(Enemy* enemy) {
	if (!enemy->getCanAttack()) {
		cout << "El sistema de orquesta no ha permitido el ataque debido a que un enemigo del mismo tipo esta atacando." << endl;
		return false;
	}
	else {
		cout << "El sistema de orquesta ha permitido el ataque." << endl;
		return true;
	}
}

void OrchestraDirector::sendMessage(Enemy* enemy, bool canAttack) {
	enemy->setCanAttack(canAttack);
}

bool OrchestraDirector::allowOrchestedAttack() {
	srand(time(0));
	int allow = rand() % 100 + 1;
	if (allow < R) {
		return true;
	}
	else {
		return false;
	}
}