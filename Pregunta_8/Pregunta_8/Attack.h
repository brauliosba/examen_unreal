#pragma once
#include <string>
#include <vector> 

using namespace std;

class Attack
{
private:
	float attackDuration;
	vector<string> attackTags;
	vector<string> reactionTags;

public:
	Attack(float attackDuration, vector<string> attackTags, vector<string> reactionTags);
	float getAttackDuration() { return attackDuration; }
	vector<string> getAttackTags() { return attackTags; }
	vector<string> getReactionTags() { return reactionTags; }
};