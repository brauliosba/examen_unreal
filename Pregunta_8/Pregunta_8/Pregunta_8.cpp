#include <conio.h>
#include "OrchestraDirector.h"

#define N 3
#define R 50
#define T 5.0

struct BlockData {
	clock_t orchestraBlockTime;
	bool successfulAttack;
	string blockedType;
	clock_t blockedTypeTime;
	clock_t attackTime;
};

BlockData handleAttack(Enemy enemy, OrchestraDirector* director, int size, Enemy activeEnemies[]) {
	cout << "El enemigo " << enemy.getName() << " ha intentado atacar" << "." << endl;
	clock_t orchestraBlockTime = 0;
	clock_t blockedTypeTime = 0;
	clock_t attackTime = 0;
	string blockedType = "  ";
	if (director->allowAttack(&enemy)) {
		blockedTypeTime = clock();
		blockedType = enemy.getEnemyType();
		attackTime = enemy.getEnemyAttacks()->getAttackDuration();
		for (int i = 0; i < size; i++) {
			if (activeEnemies[i].getName() != enemy.getName() && activeEnemies[i].getEnemyType() == enemy.getEnemyType()) {
				director->sendMessage(&activeEnemies[i], false);
			}
		}
		if (!director->getOrchestraAlive() && director->allowOrchestedAttack() && director->getCanActiveOrchestra()) {
			Attack* attacks = enemy.getEnemyAttacks();
			vector<string> attackTags = attacks->getAttackTags();
			for (int i = 0; i < size; i++) {
				if (activeEnemies[i].getName() != enemy.getName()) {
					Attack* attacks2 = activeEnemies[i].getEnemyAttacks();
					vector<string> reactionTags = attacks2->getReactionTags();
					for (int j = 0; j < attackTags.size(); j++) {
						for (int k = 0; k < reactionTags.size(); k++) {
							if (attackTags[j] == reactionTags[k]) {
								orchestraBlockTime = clock();
								director->setCanActiveOrchestra(false);
								director->setOrchestraAlive(true);
								cout << "El sistema ha organizado un ataque de orquesta entre enemigo " << enemy.getName() << " y " << activeEnemies[i].getName() << "." << endl;
							}
						}
					}
				}
			}
		}
		return { orchestraBlockTime, true, blockedType, blockedTypeTime, attackTime };
	}
	return { 0, false, "  ", 0, 0 };
}

bool existsInVector(vector<string> v, string busqueda) {
	return find(v.begin(), v.end(), busqueda) != v.end();
}

int main()
{
	cout << "Para atacar presione Q, W o T." << endl;
	cout << "Para salir de la simulacion presione P.\n" << endl;
	vector<string> temp = { "flincher" , "knockdown" };
	vector<string> temp2 = { "bullet" };
	Enemy enemy1 = Enemy("Rana", "Terrestre", new Attack(5.0, temp, temp2));

	temp = { "charger" , "distance" };
	temp2 = { "knockdown" };
	Enemy enemy2 = Enemy("Tronco", "Terrestre", new Attack(8.0, temp, temp2));

	temp = { "distance" , "bullet" };
	temp2 = { "charger" };
	Enemy enemy3 = Enemy("Mono", "Aereo", new Attack(2.0, temp, temp2));

	Enemy activeEnemies[3] = { enemy1, enemy2, enemy3 };
	int size = sizeof(activeEnemies) / sizeof(activeEnemies[0]);

	OrchestraDirector* director = new OrchestraDirector(N, R, T);

	vector<string> blockedTypes;
	vector<clock_t> blockedTypeTimes;
	vector<clock_t> attackTimes;

	clock_t currenTime = clock();
	clock_t orchestraBlockTime = 0;
	BlockData blockData = { 0, false };
	int cont = 0;
	bool startCounting = 0;

	char t;
	t = _getch();
	while (t != 'p') {
		if (cont == 0 && !director->getCanActiveOrchestra()) {
			startCounting = true;
		}
		if (t == 'q') {
			blockData = handleAttack(activeEnemies[0], director, size, activeEnemies);
			if (blockData.successfulAttack && startCounting) {
				cont++;
			}
		} 
		else if (t == 'w') {
			blockData = handleAttack(activeEnemies[1], director, size, activeEnemies);
			if (blockData.successfulAttack && startCounting) {
				cont++;
			}
		}
		else if (t == 't') {
			blockData = handleAttack(activeEnemies[2], director, size, activeEnemies);
			if (blockData.successfulAttack && startCounting) {
				cont++;
			}
		}
		else {
			cout << "Presione otra tecla (P, Q, W o T)." << endl;
		}

		if (cont == N) {
			cout << "\nEl numero minimo de ataques simples para desbloquear un ataque orquesta ha sido alcanzado.\n" << endl;
			director->setCanActiveOrchestra(true);
			startCounting = false;
			cont = 0;
		}
		if (blockData.orchestraBlockTime != 0) {
			orchestraBlockTime = blockData.orchestraBlockTime;
		}

		if (blockData.blockedTypeTime != 0 && !existsInVector(blockedTypes, blockData.blockedType)) {
			blockedTypes.push_back(blockData.blockedType);
			blockedTypeTimes.push_back(blockData.blockedTypeTime);
			attackTimes.push_back(blockData.attackTime);
		}

		currenTime = clock();
		if (orchestraBlockTime != 0) {
			if ((float)(currenTime - orchestraBlockTime) / CLOCKS_PER_SEC >= T) {
				cout << "\nLa ventana de tiempo de bloqueo de ataque en orquesta ha terminado.\n" << endl;
				director->setOrchestraAlive(false);
				orchestraBlockTime = 0;
			}
		}
		if (blockedTypes.size() != 0) {
			for (int i = 0; i < blockedTypes.size(); i++) {
				if ((float)(currenTime - blockedTypeTimes[i]) / CLOCKS_PER_SEC >= attackTimes[i]) {
					cout << "\nLa ventana de bloqueo de ataque generada por el ataque de los enemigos tipo " << blockedTypes[i] << " ha terminado.\n" << endl;
					for (int j = 0; j < size; j++) {
						if (activeEnemies[j].getEnemyType() == blockedTypes[i]) {
							director->sendMessage(&activeEnemies[j], true);
						}
					}
					blockedTypes.erase(blockedTypes.begin() + i);
					blockedTypeTimes.erase(blockedTypeTimes.begin() + i);
					attackTimes.erase(attackTimes.begin() + i);
				}
			}
		}

		t = _getch();
	}

	return 0;
}