#pragma once
#include <list>

namespace CppCLRWinformsProjekt {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	private:
		Graphics^ Graficador;
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
			Graficador = this->CreateGraphics();
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
			delete Graficador;
		}
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::TextBox^ textBox3;
	private: System::Windows::Forms::TextBox^ textBox4;

	private: System::Windows::Forms::RichTextBox^ richTextBox1;



	protected:

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(280, 60);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(100, 20);
			this->textBox1->TabIndex = 0;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(51, 194);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Graficar";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(48, 63);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(139, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Ingrese cantidad de puntos:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(48, 104);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(137, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Ingrese el tama�o del radio:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(48, 144);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(207, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"Ingrese las coordenadas del centro (x , y):";
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(280, 101);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(100, 20);
			this->textBox2->TabIndex = 5;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(280, 141);
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(100, 20);
			this->textBox3->TabIndex = 6;
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(407, 141);
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(100, 20);
			this->textBox4->TabIndex = 7;
			// 
			// richTextBox1
			// 
			this->richTextBox1->Location = System::Drawing::Point(51, 247);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->Size = System::Drawing::Size(170, 182);
			this->richTextBox1->TabIndex = 9;
			this->richTextBox1->Text = L"";
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(687, 458);
			this->Controls->Add(this->richTextBox1);
			this->Controls->Add(this->textBox4);
			this->Controls->Add(this->textBox3);
			this->Controls->Add(this->textBox2);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->textBox1);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
		if (textBox1->Text != " " && textBox2->Text != " " && textBox3->Text != " " && textBox4->Text != " ") {
			int n = int::Parse(textBox1->Text);
			String^ temp = textBox2->Text;
			float r = (float)(Convert::ToDouble(temp));
			temp = textBox3->Text;
			float x = (float)(Convert::ToDouble(temp));
			temp = textBox4->Text;
			float y = (float)(Convert::ToDouble(temp));

			PointF p(x, y);
			PointF* points = (PointF*)malloc(sizeof(PointF) * n);

			int size;
			if (n % 2 == 0) {
				size = n / 2;
			}
			else {
				size = (n - 1) / 2;
				points[size] = PointF(p.X, p.Y + r);
			}
			int cont = 0;
			float i = 0;
			float change = r / size;
			while (i < r) {
				float x = -(sqrt(pow(r, 2) - pow((i), 2))) + p.X;
				float x2 = sqrt(pow(r, 2) - pow((i), 2)) + p.X;
				points[cont] = PointF(x, i + p.Y);
				points[n - (cont + 1)] = PointF(x2, i + p.Y);
				i += change;
				cont++;
			}

			richTextBox1->Text += "Puntos del centro:\n";
			richTextBox1->Text += p.X + "\t\t" + p.Y + "\n";
			richTextBox1->Text += "\n";
			richTextBox1->Text += "Puntos de la semicircunferencia:\n";
			Graficador->DrawEllipse(Pens::Red, int(p.X + 450), int(p.Y + 200), 5, 5);
			for (int i = 0; i < n; i++) {
				richTextBox1->Text += points[i].X + "\t\t" + points[i].Y + "\n";
				Graficador->DrawEllipse(Pens::Red, int(points[i].X + 450), int(points[i].Y + 200), 5, 5);
			}
		}
		else {
			MessageBox::Show("Ingresa los valores");
		}
	}
};
}
