#include "Rectangle.h"

Rectangle::Rectangle(char name, float x, float y, float w, float h) {
	this->name = name;
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
	this->top = y + h / 2;
	this->bottom = y - h / 2;
	this->left = x - w / 2;
	this->right = x + w / 2;
}

void Rectangle::detectCollision(Rectangle A) {
	float top2 = A.getTop();
	float bottom2 = A.getBottom();
	float left2 = A.getLeft();
	float right2 = A.getRight();
	if (left2 >= left && left2 <= right) {
		if ((bottom2 >= bottom && bottom2 <= top) || (top2 >= bottom && top2 <= top) || (top2 >= top && bottom2 <= bottom)) {
			cout << "Collision between " << name << " and " << A.getName() << endl;
		}
	}
	else if (right2 >= left && right2 <= right) {
		if ((bottom2 >= bottom && bottom2 <= top) || (top2 >= bottom && top2 <= top) || (top2 >= top && bottom2 <= bottom)) {
			cout << "Collision between " << name << " and " << A.getName() << endl;
		}
	}
	else if ((bottom2 >= bottom && bottom2 <= top) || (top2 >= bottom && top2 <= top) || (top2 >= top && bottom2 <= bottom)) {
		cout << "Collision between " << name << " and " << A.getName() << endl;
	}
}