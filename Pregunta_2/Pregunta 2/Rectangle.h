#include <iostream>
using namespace std;

#pragma once
class Rectangle
{
private:
	char name;
	float x, y, w, h;
	float top, bottom, left, right;
public:
	Rectangle(char name, float x, float y, float w, float h);
	char getName() { return name; }
	float getX() { return x; }
	float getY() { return y; }
	float getW() { return w; }
	float getH() { return h; }
	float getTop() { return top; }
	float getBottom() { return bottom; }
	float getLeft() { return left; }
	float getRight() { return right; }
	void detectCollision(Rectangle r);
};

