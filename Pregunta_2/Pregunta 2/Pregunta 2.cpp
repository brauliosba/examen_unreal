#include <array>
#include "Rectangle.h"

int main()
{
    Rectangle a('A', 7, 4, 6, 4);
    Rectangle b('B', 10, 10, 4, 2);
    Rectangle c('C', 10, 8, 4, 6);

    Rectangle rectangles[] = { a, b, c };
    int size = *(&rectangles + 1) - rectangles;
    
    for (int i = 0; i < size; i++) {
        for (int j = i + 1; j < size; j++) {
            rectangles[i].detectCollision(rectangles[j]);
        }
    }

    return 0;
}
